from django.urls import path
from . import views

app_name = 'todo'

urlpatterns = [
    path('', views.index, name='index'),
    path('todo_list_partial', views.todo_list_partial, name='todo_list_partial'),
    path('create/', views.create, name='create'),
    path('todo_details_partial/<int:pk>', views.todo_details_partial, name='todo_details_partial'),
    path('rerank', views.rerank, name="rerank"),
]
