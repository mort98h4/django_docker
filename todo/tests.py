from django.test import TestCase, Client
from .models import Todo


class TodoTestCase(TestCase):

    def setUp(self):
        Todo.create(title='Kill the cat', text='Use the chainsaw.')
        Todo.create(title='Feed the dog', text='He likes steak.')
        Todo.create(title='Finish making tests', text='We need to cover all essential code.')
        Todo.create(title='Make a tutorial video', text='')
        Todo.create(title='', text='Come up with a title for this item.')
        Todo.create(title='', text='')
        Todo.create(title='Browser tests', text='Safari, Firefox, Brave')

    def test_default_order(self):
        kill_cat = Todo.objects.get(title='Kill the cat')
        feed_dog = Todo.objects.get(title='Feed the dog')
        assert kill_cat.rank < feed_dog.rank

    def test_highest_rank(self):
        browser_tests = Todo.objects.get(title='Browser tests')
        assert browser_tests.rank == Todo.highest_rank

    def test_rerank_up(self):
        make_video = Todo.objects.get(title='Make a tutorial video')
        make_video.rerank(1)
        make_video.refresh_from_db()
        kill_cat = Todo.objects.get(title='Kill the cat')
        assert make_video.rank == 1
        assert kill_cat.rank == 2

    def test_rerank_down(self):
        kill_cat = Todo.objects.get(title='Kill the cat')
        kill_cat.rerank(4)
        kill_cat.refresh_from_db()
        assert kill_cat.rank == 4
        finish_test = Todo.objects.get(title='Finish making tests')
        assert finish_test.rank == 2

    def test_rerank_by_list(self):
        reranked = [todo.pk for todo in Todo.objects.all()]
        first = reranked[0]
        last = reranked.pop()
        reranked.insert(0, last)
        Todo.rerank_by_list(reranked)
        assert Todo.objects.get(pk=last).rank == 1
        assert Todo.objects.get(pk=first).rank == 2

    def test_empty_fields(self):
        empty_title = Todo.objects.filter(title='')[0]
        assert empty_title.text == 'Come up with a title for this item.'

    def test_load_index(self):
        c = Client()
        response = c.get('/')
        self.assertTemplateUsed(response, 'todo/index.html')
        assert response.status_code == 200
        assert response['content-type'] == 'text/html; charset=utf-8'

    def test_load_item(self):
        kill_cat = Todo.objects.get(title='Kill the cat').pk
        c = Client()
        response = c.get(f'/todo_details_partial/{kill_cat}')
        assert b"Kill the cat" in response.content

    def test_add_item(self):
        c = Client(HTTP_HX_CURRENT_URL='/')
        response = c.post('/create/', {'title': 'My new item', 'text': 'Some text...'})
        assert response.status_code == 200
        response = c.get('/todo_details_partial/8')
        assert b'My new item' in response.content
